package main

import (
	"math"
)

type gain struct {
	a int
	b int
}

type matrix struct {
	row int
	col int
	gains []gain
}

func (m *matrix) getIndex(row, col int) int {
	return (col - 1) + m.col * (row - 1)
}

func (m *matrix) getCell(row, col int) (g *gain) {
	g = &m.gains[m.getIndex(row, col)]
	return
}

func (m *matrix) getRow(row int) (gains []*gain) {
	for col := 1; col <= m.col; col++ {
		gains = append(gains, m.getCell(row, col))
	}

	return gains
}

func (m *matrix) getCol(col int) (gains []*gain) {
	for row := 1; row <= m.row; row++ {
		gains = append(gains, m.getCell(row, col))
	}

	return gains
}

type pair struct {
	row int
	col int
}

func (m *matrix) getNash() (pairs []pair) {
	pairs = make([]pair, 0)

	// Bに対してのAの最適反応戦略
	bestA := make([]pair, 0)
	for col := 1; col <= m.col; col++ {
		max := math.MinInt64

		for _, gain := range m.getCol(col) {
			if max < gain.a {
				max = gain.a
			}
		}

		for i, gain := range m.getCol(col) {
			if gain.a == max {
				bestA = append(bestA, pair{i + 1, col})
			}
		}
	}

	// Aに対してのBの最適反応戦略
	bestB := make([]pair, 0)
	for row := 1; row <= m.row; row++ {
		max := math.MinInt64

		for _, gain := range m.getRow(row) {
			if max < gain.b {
				max = gain.b
			}
		}

		for i, gain := range m.getRow(row) {
			if gain.b == max {
				bestB = append(bestB, pair{row, i + 1})
			}
		}
	}

	// A最適反応とB最適反応の両方にあるものがナッシュ均衡解
	for _, pb := range bestB {
		if pairContains(bestA, pb) {
			if !pairContains(pairs, pb) {
				pairs = append(pairs, pb)
			}
		}
	}

	return pairs
}

func pairContains(pairs []pair, pair pair) bool {
	for _, p := range pairs {
		if pair == p {
			return true
		}
	}

	return false
}
