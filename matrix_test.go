package main

import (
	"testing"
	"reflect"
)

var (
	m1 matrix
	m2 matrix
	m3 matrix
	m4 matrix
	m5 matrix
	m6 matrix
	m7 matrix
	m8 matrix
)

func init() {
	m1 = matrix{
		col: 2,
		row: 2,
		gains: []gain{
			{40, 45}, {30, 50},
			{50, 35}, {20, 25},
		},
	}

	m2 = matrix{
		col: 2,
		row: 4,
		gains: []gain{
			{40, 45}, {30, 50},
			{50, 35}, {20, 25},
			{35, 20}, {45, 35},
			{45, 50}, {25, 40},
		},
	}

	m3 = matrix{
		col: 3,
		row: 3,
		gains: []gain{
			{40, 45}, {30, 50}, {50, 35},
			{20, 25}, {35, 20}, {45, 30},
			{45, 50}, {25, 40}, {30, 25},
		},
	}

	m4 = matrix{
		col: 3,
		row: 3,
		gains: []gain{
			{5, 2}, {2, 4}, {4, 5},
			{4, 6}, {3, 6}, {2, 5},
			{3, 3}, {1, 2}, {7, 2},
		},
	}

	// 囚人のジレンマ
	m5 = matrix{
		col: 2,
		row: 2,
		gains: []gain{
			{-2, -2}, {-10, 0},
			{0, -10}, {-5, -5},
		},
	}

	m6 = matrix{
		col: 2,
		row: 2,
		gains: []gain{
			{10, 20}, {20, 10},
			{20, 10}, {10, 20},
		},
	}

	// じゃんけん
	m7 = matrix{
		col: 3,
		row: 3,
		gains: []gain{
			{0, 0}, {1, -1}, {1, -1},
			{-1, 1}, {0, 0}, {-1, 1},
			{1, -1}, {-1, 1}, {0, 0},
		},
	}

	// 全て
	m8 = matrix{
		col: 3,
		row: 3,
		gains: []gain{
			{0, 0}, {0, 0}, {0, 0},
			{0, 0}, {0, 0}, {0, 0},
			{0, 0}, {0, 0}, {0, 0},
		},
	}
}

func TestMatrix_getCol(t *testing.T) {
	testTable := []struct{
		m matrix
		p int
		expect []gain
	}{
		{m1, 1, []gain{{40, 45}, {50, 35}}},
		{m1, 2, []gain{{30, 50}, {20, 25}}},
		{m2, 1, []gain{{40, 45}, {50, 35}, {35, 20}, {45, 50}}},
		{m2, 2, []gain{{30, 50}, {20, 25}, {45, 35}, {25, 40}}},
		{m3, 1, []gain{{40, 45}, {20, 25}, {45, 50}}},
		{m3, 2, []gain{{30, 50}, {35, 20}, {25, 40}}},
		{m3, 3, []gain{{50, 35}, {45, 30}, {30, 25}}},
	}

	for i, test := range testTable {
		actual := test.m.getCol(test.p)

		if len(actual) != len(test.expect) {
			t.Fatalf("TestNo.%d expected len %d, bat was %d", i+1, len(test.expect), len(actual))
		}

		for j, expectGain := range test.expect {
			if *actual[j] != expectGain {
				t.Fatalf("TestNo.%d expected %+v, bat was %+v", i+1, expectGain, *actual[j])
			}
		}

		t.Logf("TestNo.%d success", i+1)
	}
}

func TestMatrix_getRow(t *testing.T) {
	testTable := []struct{
		m matrix
		p int
		expect []gain
	}{
		{m1, 1, []gain{{40, 45}, {30, 50}}},
		{m1, 2, []gain{{50, 35}, {20, 25}}},
		{m2, 1, []gain{{40, 45}, {30, 50}}},
		{m2, 2, []gain{{50, 35}, {20, 25}}},
		{m2, 3, []gain{{35, 20}, {45, 35}}},
		{m2, 4, []gain{{45, 50}, {25, 40}}},
		{m3, 1, []gain{{40, 45}, {30, 50}, {50, 35}}},
		{m3, 2, []gain{{20, 25}, {35, 20}, {45, 30}}},
		{m3, 3, []gain{{45, 50}, {25, 40}, {30, 25}}},
	}

	for i, test := range testTable {
		actual := test.m.getRow(test.p)

		if len(actual) != len(test.expect) {
			t.Fatalf("TestNo.%d expected len %d, bat was %d", i+1, len(test.expect), len(actual))
		}

		for j, expectGain := range test.expect {
			if *actual[j] != expectGain {
				t.Fatalf("TestNo.%d expected %+v, bat was %+v", i+1, expectGain, *actual[j])
			}
		}

		t.Logf("TestNo.%d success", i+1)
	}
}

func TestMatrix_getIndex(t *testing.T) {
	type params struct {
		row int
		col int
	}

	testTable := []struct{
		m matrix
		p params
		expect int
	}{
		{m1, params{1, 1}, 0},
		{m1, params{1, 2}, 1},
		{m1, params{2, 1}, 2},
		{m1, params{2, 2}, 3},
		{m2, params{1, 1}, 0},
		{m2, params{1, 2}, 1},
		{m2, params{2, 1}, 2},
		{m2, params{2, 2}, 3},
		{m2, params{3, 1}, 4},
		{m2, params{3, 2}, 5},
		{m2, params{4, 1}, 6},
		{m2, params{4, 2}, 7},
		{m3, params{1, 1}, 0},
		{m3, params{1, 2}, 1},
		{m3, params{1, 3}, 2},
		{m3, params{2, 1}, 3},
		{m3, params{2, 2}, 4},
		{m3, params{2, 3}, 5},
		{m3, params{3, 1}, 6},
		{m3, params{3, 2}, 7},
		{m3, params{3, 3}, 8},
	}

	for i, test := range testTable {
		actual := test.m.getIndex(test.p.row, test.p.col)

		if actual != test.expect {
			t.Fatalf("TestNo.%d expected %+v, bat was %+v", i+1, test.expect, actual)
		}

		t.Logf("TestNo.%d success", i+1)
	}
}

func TestMatrix_getCell(t *testing.T) {
	type params struct {
		row int
		col int
	}

	testTable := []struct{
		m matrix
		p params
		expect gain
	}{
		{m1, params{1, 1}, gain{40, 45}},
		{m1, params{1, 2}, gain{30, 50}},
		{m1, params{2, 1}, gain{50, 35}},
		{m1, params{2, 2}, gain{20, 25}},
		{m2, params{1, 1}, gain{40, 45}},
		{m2, params{1, 2}, gain{30, 50}},
		{m2, params{2, 1}, gain{50, 35}},
		{m2, params{2, 2}, gain{20, 25}},
		{m2, params{3, 1}, gain{35, 20}},
		{m2, params{3, 2}, gain{45, 35}},
		{m2, params{4, 1}, gain{45, 50}},
		{m2, params{4, 2}, gain{25, 40}},
		{m3, params{1, 1}, gain{40, 45}},
		{m3, params{1, 2}, gain{30, 50}},
		{m3, params{1, 3}, gain{50, 35}},
		{m3, params{2, 1}, gain{20, 25}},
		{m3, params{2, 2}, gain{35, 20}},
		{m3, params{2, 3}, gain{45, 30}},
		{m3, params{3, 1}, gain{45, 50}},
		{m3, params{3, 2}, gain{25, 40}},
		{m3, params{3, 3}, gain{30, 25}},
	}

	for i, test := range testTable {
		actual := test.m.getCell(test.p.row, test.p.col)

		if *actual != test.expect {
			t.Fatalf("TestNo.%d expected %+v, bat was %+v", i+1, test.expect, *actual)
		}

		t.Logf("TestNo.%d success", i+1)
	}
}

func TestMatrix_getNash(t *testing.T) {
	testTable := []struct{
		m matrix
		expect []pair
	}{
		{m1, []pair{{1, 2}, {2, 1}}},
		{m2, []pair{{2, 1}, {3, 2}}},
		{m3, []pair{{3, 1}}},
		{m4, []pair{{2, 2}}},
		{m5, []pair{{2, 2}}},
		{m6, []pair{}},
		{m7, []pair{}},
		{m8, []pair{
			{1, 1}, {1, 2}, {1, 3},
			{2, 1}, {2, 2}, {2, 3},
			{3, 1}, {3, 2}, {3, 3},}},
	}

	for i, test := range testTable {
		actual := test.m.getNash()

		if !reflect.DeepEqual(test.expect, actual) {
			t.Fatalf("TestNo.%d expected %+v, bat was %+v", i+1, test.expect, actual)
		}

		t.Logf("TestNo.%d success", i+1)
	}
}

func TestPairContains(t *testing.T) {
	type params struct {
		pairs []pair
		pair pair
	}

	testTable := []struct{
		p params
		expect bool
	}{
		{params{[]pair{}, pair{1, 1}}, false},
		{params{[]pair{{1, 2}}, pair{1, 1}}, false},
		{params{[]pair{{2, 1}}, pair{1, 1}}, false},
		{params{[]pair{{1, 1}}, pair{1, 1}}, true},
		{params{[]pair{{1, 1}, {1, 1}}, pair{1, 1}}, true},
		{params{[]pair{{1, 2}, {2, 1}, {2, 2}}, pair{1, 1}}, false},
	}

	for i, test := range testTable {
		actual := pairContains(test.p.pairs, test.p.pair)

		if actual != test.expect {
			t.Fatalf("TestNo.%d expected %+v, bat was %+v", i+1, test.expect, actual)
		}

		t.Logf("TestNo.%d success", i+1)
	}
}
