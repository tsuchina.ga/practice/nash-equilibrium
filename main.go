package main

import "log"

func main() {
	m := matrix{}

	log.Printf("利得行列1")
	m = matrix{
		col: 3,
		row: 3,
		gains: []gain{
			{40, 45}, {30, 50}, {50, 35},
			{20, 25}, {35, 20}, {45, 30},
			{45, 50}, {25, 40}, {30, 25},
		},
	}
	log.Printf("%+v", m.getNash())

	log.Printf("利得行列2")
	m = matrix{
		col: 2,
		row: 4,
		gains: []gain{
			{40, 45}, {30, 50},
			{50, 35}, {20, 25},
			{35, 20}, {45, 35},
			{45, 50}, {25, 40},
		},
	}
	log.Printf("%+v", m.getNash())

	log.Printf("利得行列3")
	m = matrix{
		col: 2,
		row: 2,
		gains: []gain{
			{-2, -2}, {-10, 0},
			{0, -10}, {-5, -5},
		},
	}
	log.Printf("%+v", m.getNash())

	log.Printf("利得行列4")
	m = matrix{
		col: 3,
		row: 3,
		gains: []gain{
			{0, 0}, {1, -1}, {1, -1},
			{-1, 1}, {0, 0}, {-1, 1},
			{1, -1}, {-1, 1}, {0, 0},
		},
	}
	log.Printf("%+v", m.getNash())

	log.Printf("利得行列5")
	m = matrix{
		col: 3,
		row: 3,
		gains: []gain{
			{0, 0}, {0, 0}, {0, 0},
			{0, 0}, {0, 0}, {0, 0},
			{0, 0}, {0, 0}, {0, 0},
		},
	}
	log.Printf("%+v", m.getNash())
}
